import { Component } from "@angular/core"

@Component({
  selector: 'app-contador',
  template: `
    <h1>{{titulo}}</h1>
    <span>La base es {{base}}</span>
    <br>
    <br>
    <button (click)=acumular(base)>+ {{base}}</button>

    <span>{{numero}}</span> 

    <button (click)="acumular(-base)">- {{base}}</button>
  `
})

export class ContadorComponent {
  title:string = 'Bases';
  titulo:string = "Contador";
  numero:number = 10;

  base:number = 5;

  acumular(valor:number){
    this.numero +=valor;
  }
}