import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
})
export class ListadoComponent {
  
  heroes: string[] = ['Ironman', 'Spiderman', 'Hulk', 'Thor', 'Capitán América'];
  heroRemoved: string = '';

  borrarHeroe():void {
    console.log('Borrando...');
    this.heroRemoved = this.heroes.shift() || ''
    console.log(this.heroes);
  }
}
